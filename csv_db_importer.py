#!/usr/bin/python3
# -*- coding: UTF-8 -*-

from common_lib.db_importer_util import DbImporterUtil
import sys
from common_lib.common_error import BadUserInputError
from common_lib.common_base import get_input_parameter_value, valid_mandatory_parameters
import os

                



def start(argv):
    if (('-h' in argv) or ('-?' in argv)):
        print("""
        python3 csv_db_importer.py -l LOG_FILE_PATH
        Program to import a pingmonitor log file to a mysql database
        Parameters:
            -l LOG_FILE_PATH  --> required, specify the path to log file
        """)
    
    elif not valid_mandatory_parameters(argv, ['-l']):
        BadUserInputError("Input error. To run, call as python3 csv_db_importer.py --l LOG_FILE_PATH, try -h to see the options available")
    else:
        log_file_path = get_input_parameter_value(argv,'-l')
        importing_file_path = log_file_path + ".importing"
        #check if log_file_path exists
        if not os.path.exists(log_file_path):
            if not os.path.exists(importing_file_path):
                raise BadUserInputError(f"Log file path {log_file_path} does not exist") 
        
        
        if not os.path.exists(importing_file_path):
            #rename file with postfix .importing
            os.rename(log_file_path, importing_file_path)

        db_importer_util = DbImporterUtil(importing_file_path)
        db_importer_util.import_csv_to_mysql()




start(sys.argv)
