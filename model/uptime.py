#!/usr/bin/python3
# -*- coding: UTF-8 -*-

from datetime import datetime
class Uptime(object):
    def __init__(self, 
                 dat_hor_inicio:datetime, 
                 dat_hor_fim:datetime,
                 status:bool):
        self.dat_hor_inicio:datetime = dat_hor_inicio
        self.dat_hor_fim :datetime= dat_hor_fim
        self.status:bool = status