#!/usr/bin/python3
# -*- coding: UTF-8 -*-

from common_lib.common_base import get_input_parameter_value, valid_mandatory_parameters
from common_lib.common_error import BadUserInputError
from connection_monitor import ConnectionMonitor
import sys

def start(argv):
#    print(argv)
    if (('-h' in argv) or ('-?' in argv)):
        print("""
        python3 pingmonitor.py -t TARGET_NAME_OR_IP [-f CSV_FILENAME] [-p TARGET_PORT] [-r INTERVAL_IN_SECOUNDS_TO_REPEAT]
        Program do check is a host is online sending a SYN package or a ICMP package (ping)
        Parameters:
            -t TARGET_NAME_OR_IP  --> required, specify the target FQDN or IP to be pinged
            -p TARGET_PORT --> optional, specify the target port, in case to try send TCP SYN package to an specific port
                                when not informed, a ICMP package will be send. 
            -f CSVFILENAME --> optional, specify the csv filename where detailes of monitoring will be written for histpry record
            -i INTERVAL_IN_SECONDS_TO_REPEAT --> optional, if specified, the monitoring will continue eatch especifyed interval
            -s ACTION_COMMAND_CASE_ONLINE --> optional, specify command to be executed in case the monitor check returns up
            -x ACTION_COMMAND_CASE_OFFLINE --> optional, specify command to be executed in case the monitor check returns down
            -w PING_TIMEOUT_IN_SECONDS --> optional, specify timeout in seconds for ping. The default value is 2 secounds
            -debug True|False--> optionnal, if not specified, the program will assume False
        """)
    
    elif ((len(argv) < 3) and not ('-t' in argv)):
        BadUserInputError("Input error. To run, call as python3 pingmonitor.py -t TARGET_NAME_OR_IP [options], try -h to see the options available")
    else:
        hostname = get_input_parameter_value(argv,'-t')
        port = get_input_parameter_value(argv,'-p')
        csv_filepath = get_input_parameter_value(argv,'-f')
        interval_to_repeat = get_input_parameter_value(argv,'-i')
        up_action = get_input_parameter_value(argv,'-s')
        down_action = get_input_parameter_value(argv,'-x')
        ping_timeout = get_input_parameter_value(argv,'-w')
        debug_param =  get_input_parameter_value(argv,'-debug')
        debug = False if debug_param is None else bool(debug_param)
        if (ping_timeout is None):
            ping_timeout = 2 #default timeout

        connectionMonitor = ConnectionMonitor(hostname= hostname,
                                                port = port,
                                                ping_timeout = ping_timeout,
                                                up_action = up_action,
                                                down_action= down_action,
                                                interval_in_secounds = interval_to_repeat,
                                                csv_filepath = csv_filepath,
                                                debug=debug)
        connectionMonitor.start_monitor()

start(sys.argv)
