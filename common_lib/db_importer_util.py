#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import csv
from model.uptime import Uptime
from  datetime import  datetime, timedelta
from decouple import config
import os
import mysql.connector
import socket
import mysql
import codecs


class DbImporterUtil(object):

    MYSQL_HOSTNAME = config('MYSQL_HOSTNAME')
    MYSQL_PORT = config('MYSQL_PORT')
    MYSQL_DATABASE = config('MYSQL_DATABASE')
    MYSQL_USERNAME = config('MYSQL_USERNAME')
    MYSQL_PASSWORD = config('MYSQL_PASSWORD')

    def __init__(self, csv_file_path:str):
        if not os.path.exists(csv_file_path):
            raise FileNotFoundError(f"Log file path {csv_file_path} does not exist") 
        
        self.csv_file_path = csv_file_path
        #full hostname from current machine
        self.hostname = socket.gethostname()
        self.__connection:mysql.connector.connection.MySQLConnection  = None
        #create cursor
        self.id_monitor_host:int = self.__get_id_monitor_host()



    def __get_connection(self):
        
            if self.__connection is None or self.__connection.is_closed():
                self.__connection = mysql.connector.connect(host=self.MYSQL_HOSTNAME,
                                                        port=self.MYSQL_PORT,
                                                        database=self.MYSQL_DATABASE,
                                                        user=self.MYSQL_USERNAME,
                                                        password=self.MYSQL_PASSWORD)
            
            return self.__connection
            
    def __close_connection(self, 
                           cursor:mysql.connector.connection.MySQLCursor, 
                           connection:mysql.connector.connection.MySQLConnection):
        if cursor is not None:
            cursor.close()
        if connection is not None and connection.is_connected():
            connection.close()

    def __get_id_monitor_host(self) -> int:
        id_monitor_host:int
        # select id_monitor_host from table tb001_monitor_hosts using full hostname
        sql_statemant = "SELECT id_monitor_host FROM tb001_monitor_hosts WHERE nome_monitor_host = %s"
        try:
            connection = self.__get_connection()
            cursor = self.__get_connection().cursor(buffered=True)
            cursor.execute(sql_statemant, (self.hostname,))
            #check if exists, if not, insert new hostname to table tb001_monitor_hosts
            if cursor.rowcount == 0:
                sql_statemant = "INSERT INTO tb001_monitor_hosts (nome_monitor_host) VALUES (%s)"
                cursor.execute(sql_statemant, (self.hostname,))
                connection.commit()
                #get last inserted id
                id_monitor_host = cursor.lastrowid
            else:
                id_monitor_host = cursor.fetchone()[0]
        finally:
            self.__close_connection(cursor=cursor, connection=connection)
        return id_monitor_host

    #function to cast list[Uptime] to list[tuple[datetime, datetime, bool]]
    def cast_uptime_list_to_tuple_list(self, up_time_list:list[Uptime]) -> list[tuple[datetime, datetime, bool, int]]:
        up_time_tuple_list:list[tuple[datetime, datetime, bool, int]] = []
        for up_time in up_time_list:
            up_time_tuple_list.append((up_time.dat_hor_inicio, 
                                       up_time.dat_hor_fim, 
                                       up_time.status, 
                                       self.id_monitor_host))
        return up_time_tuple_list

    def insert_uptime_list(self, up_time_list:list[Uptime]):
        try:
            connection = self.__get_connection()
            cursor = self.__get_connection().cursor(buffered=True)
            #insert sql_statemant with specific types
            sql_statemant = "INSERT IGNORE INTO tb005_uptime (dat_hor_inicio, dat_hor_fim, status, id_monitor_host) VALUES (%s, %s, %s, %s)"
            cursor.executemany(sql_statemant, self.cast_uptime_list_to_tuple_list(up_time_list))
            connection.commit()
        finally:
            self.__close_connection(cursor=cursor, connection=connection)

    def update_last_uptime(self, last_id_uptime:int, dat_hor_fim: datetime):
        try:
            connection = self.__get_connection()
            cursor = self.__get_connection().cursor(buffered=True)
            #update last uptime
            sql_statemant = "UPDATE tb005_uptime SET dat_hor_fim = %s WHERE id_uptime = %s"
            cursor.execute(sql_statemant, (dat_hor_fim, last_id_uptime))
            connection.commit()
        finally:
            self.__close_connection(cursor=cursor, connection=connection)

    def get_last_uptime(self, rows:list[Uptime]) -> tuple[int, bool]:
        try:
            last_id_uptime:int = None
            last_status:bool = None
            connection = self.__get_connection()
            cursor = self.__get_connection().cursor(buffered=True)
            # select last reford fromtable tb005_uptime
            sql_statemant = """SELECT id_uptime, status 
                                FROM tb005_uptime 
                                WHERE id_monitor_host = %s
                                ORDER BY id_uptime DESC LIMIT 1"""
            cursor.execute(sql_statemant, (self.id_monitor_host,))
            #get values if exists casting status column to last_status bool
            if cursor.rowcount > 0:
                last_id_uptime, last_status = cursor.fetchone()

        finally:
            self.__close_connection(cursor=cursor, connection=connection)

        return (last_id_uptime, bool(last_status))


    #function to import csv file to mysql database
    def import_csv_to_mysql(self):
        try:
            cursor = None
            connection = None
            #open csv file
            with open(self.csv_file_path, 'r') as csv_file:
                csv_reader = csv.reader((row.replace('\0', '') for row in csv_file), delimiter=';')
                #connect to mysql database
                rows:list[tuple] = []
                for row in csv_reader:
                    if len(row) == 4:
                        rows.append((datetime.strptime(row[0], '%d/%m/%Y %H:%M:%S'), #created_at
                                    self.id_monitor_host, #id_monitor_host
                                    None if row[2].strip() == "" else float(row[2]), #time_elapsed_ms
                                    False if row[-1].strip().upper() == "DOWN" else True,)) #status

                connection = self.__get_connection()
                cursor = self.__get_connection().cursor()
                #insert sql_statemant with specific types
                sql_statemant = "INSERT IGNORE INTO tb004_ping_results (created_at, id_monitor_host, time_elapsed_ms, status) VALUES (%s, %s, %s, %s)"

                #insert csv data to mysql database
                cursor.executemany(sql_statemant, rows)
                connection.commit()

                self.save_uptime(rows)


                #remove file
                os.remove(self.csv_file_path)

        except Exception as e:
            raise e
        finally:
            self.__close_connection(cursor=cursor, connection=connection)

    def save_uptime(self, rows):
        up_time_list:list[Uptime]  = []
        last_id_uptime: int = None
        last_status: bool = None
        last_uptime: Uptime = None
        
        for row in rows:
            current_status: bool  = row[3]
            if len(up_time_list) == 0:
                last_uptime = Uptime(dat_hor_inicio=row[0], 
                                     dat_hor_fim=row[0], 
                                     status=row[3])
                up_time_list.append(last_uptime)
            else:
                if current_status == last_uptime.status:
                    last_uptime.dat_hor_fim = row[0]
                else:
                    last_uptime.dat_hor_fim = row[0] - timedelta(seconds=1)
                    last_uptime = Uptime(dat_hor_inicio=row[0], 
                                               dat_hor_fim=row[0], 
                                               status=row[3])
                    up_time_list.append(last_uptime)

        if len(up_time_list) > 0:
            (last_id_uptime, last_status) = self.get_last_uptime(rows=rows)
            if last_id_uptime is not None:
                if up_time_list[0].status == last_status:
                    self.update_last_uptime(last_id_uptime=last_id_uptime, dat_hor_fim= up_time_list[0].dat_hor_fim)
                else:
                    dat_hor_fim = up_time_list[0].dat_hor_fim - timedelta(seconds=1)
                    self.update_last_uptime(last_id_uptime=last_id_uptime, dat_hor_fim=dat_hor_fim)
                    self.insert_uptime_list(up_time_list = [up_time_list[0]])
                
                if len(up_time_list) > 1:
                    #insert uptimelist from second row
                    self.insert_uptime_list(up_time_list = up_time_list[1:] )
            else:
                self.insert_uptime_list(up_time_list = up_time_list)

    