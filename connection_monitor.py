1#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import csv
import os
import time
import shutil
from tempfile import NamedTemporaryFile
from datetime import datetime
from pythonping import ping
from retrying import retry, RetryError
import socket
CSV_DELIMMITER = ';'
DATETIME_FORMAT = "%d/%m/%Y %H:%M:%S"
STATUS_UP = "up"
STATUS_DOWN = "down"

class ConnectionMonitor(object):

    def __init__(self, 
                    hostname: str,
                    port: int,
                    ping_timeout: int,
                    up_action: str,
                    down_action: str,
                    interval_in_secounds: int,
                    csv_filepath: str,
                    debug: bool = False) -> None:
        self._hostname = hostname
        self._port = port
        self._ping_timeout = ping_timeout
        self._up_action = up_action
        self._down_action = down_action
        self._interval_in_secounds = interval_in_secounds
        self._csv_filepath = csv_filepath
        self.__last_written_row = None
        self._debug = debug
        if self._debug:
            self.ping_tries_count:int = 0


    def get_line_to_write(self, record_id, start_date_time, end_date_time, ping_count, time_elapsed_ms, status):
        time_interval = end_date_time - start_date_time
        return [record_id, #0
                    start_date_time.strftime(DATETIME_FORMAT), #1
                    end_date_time.strftime(DATETIME_FORMAT), #2
                    time_interval, #6
                    self._hostname, #4
                    ping_count, #5
                    time_elapsed_ms, #6
                    status] #7

    def write_line(self, line_to_write, csv_writer):
        line_to_write = self.get_line_to_write(record_id = line_to_write[0],
                                            start_date_time = line_to_write[1], 
                                            end_date_time = line_to_write[2], 
                                            ping_count = line_to_write[5], 
                                            time_elapsed_ms = line_to_write[6], 
                                            status = line_to_write[7])
        self.write_line_to_csv(line_to_write, csv_writer = csv_writer)

    def write_line_from_current_status(self, record_id, start_date_time, ping_count, current_status, csv_writer):
        line_to_write = self.get_line_to_write(record_id = record_id,
                                            start_date_time = start_date_time,
                                            end_date_time = current_status[0],
                                            ping_count = ping_count,
                                            time_elapsed_ms = current_status[1],
                                            status = current_status[2])
        self.write_line_to_csv(line_to_write, csv_writer = csv_writer)                                   
    

    def write_line_to_csv(self, line_to_write, csv_writer):
        csv_writer.writerow(line_to_write)
        print(line_to_write)
        #print(line_to_write[0] + " - " + line_to_write[1] + " - " + line_to_write[2] + " - " + line_to_write[3] + " - " + line_to_write[4])

    @retry(retry_on_result=lambda res: (res[2] == STATUS_DOWN), stop_max_attempt_number=5, wait_random_min=0, wait_random_max=1000 )
    def read_current_status(self):
        if self._debug:
            self.ping_tries_count +=1
            #start stopwatch to meansure execution time
            start_time = time.time()


        host_status = STATUS_DOWN
        time_elapsed_ms = ""
        try:
            
            date_time =  datetime.now()
            if (self._port == None):
                response_list = ping(self._hostname, timeout=self._ping_timeout, count=1)
                if response_list._responses[0].error_message == None:
                    host_status = STATUS_UP
                    time_elapsed_ms = response_list._responses[0].time_elapsed_ms
            else:
                # see if we can resolve the host name -- tells us if there is
                # a DNS listening
                socket.gethostbyname(self._hostname)
                # connect to the host -- tells us if the host is actually
                # reachable
                s = socket.create_connection((self._hostname, self._port), 2)
                s.close()
                microseconds = datetime.now().microsecond - date_time.microsecond
                host_status = STATUS_UP
                time_elapsed_ms = microseconds / 1000

        except Exception as err:
            if self._debug:
                print(f"Unexpected {err}, {type(err)}")
            pass

        if self._debug:
            #meansure execution time
            end_time = time.time()
            time_elapsed_ms = round((end_time - start_time) * 1000, 2)
            print(f"Try {self.ping_tries_count} - Execution time: {time_elapsed_ms} ms")
            
        return (date_time, time_elapsed_ms, host_status)

    def generate_csv_file(self) -> None: 

        file_exists = False
        if not os.path.exists(self._csv_filepath):
            self._csv_filepath = os.path.join(os.path.dirname(__file__), self._csv_filepath)
        else:
            file_exists = True
        
        self.__last_written_row = None    
        if file_exists:  
            tempfile = NamedTemporaryFile('w+t', newline='', delete=False)
            try:
                with open(self._csv_filepath, 'r', newline='') as csvFile, tempfile:
                    reader = csv.reader((line.replace('\0', '') for line in csvFile), delimiter=CSV_DELIMMITER, quotechar='"')
                    writer = csv.writer(tempfile, delimiter=CSV_DELIMMITER, quotechar='"')
                    
                    for row in reader:
                        if self.__last_written_row != None:
                            writer.writerow(self.__last_written_row)
                        self.__last_written_row = row   

                shutil.move(tempfile.name, self._csv_filepath)
            finally:
                if os.path.exists(tempfile.name):
                    os.remove(tempfile.name) 

    def write_new_status(self, current_status, csv_writer):
        record_id = 1

        if not self.__last_written_row:
            self.write_line_from_current_status(record_id = record_id,
                                            start_date_time = current_status[0], 
                                            ping_count = 1,
                                            current_status = current_status, 
                                            csv_writer = csv_writer) 
        else:
            last_row_alread_written = False
            try:
                
                self.__last_written_row[1] = datetime.strptime(self.__last_written_row[1], DATETIME_FORMAT) #start_date_time
                self.__last_written_row[2] = datetime.now() #end_date_time

                if self.__last_written_row[7] == current_status[2]:
                    self.__last_written_row[5] = int(self.__last_written_row[5]) + 1 #add 1 to ping_count
                    avg_time_elapsed_ms = ""
                    if (current_status[2] == STATUS_UP):
                        avg_time_elapsed_ms = round((float(self.__last_written_row[6]) + current_status[1]) / 2, 2)
                    self.__last_written_row[6] = avg_time_elapsed_ms
                    last_row_alread_written = self.write_last_row(csv_writer)
                else:
                    record_id += int(self.__last_written_row[0]) #add 1 to record_id
                    last_row_alread_written = self.write_last_row(csv_writer)
                    self.write_line_from_current_status(record_id = record_id, 
                                                    start_date_time = current_status[0], 
                                                    ping_count = 1,
                                                    current_status = current_status, 
                                                    csv_writer=csv_writer)
            except Exception:
                self.write_line(csv_writer)
                raise

    def write_last_row(self, csv_writer):
        try:
            self.write_line(self.__last_written_row, csv_writer)
            return True
        except Exception:
            return False

    def start_monitor(self):
        if (self._interval_in_secounds):
            print("To stop press CTRL + C")

        while True:
            current_status = None
            try:
                current_status = self.read_current_status()
            except RetryError as ex:
                current_status = ex.last_attempt.value

            if (not self._csv_filepath is None):
                self.generate_csv_file()
                
                with open(self._csv_filepath, 'a+', newline='') as csvfile:
                    csv_writter = csv.writer(csvfile, delimiter=CSV_DELIMMITER,
                                                quotechar='"', quoting=csv.QUOTE_MINIMAL)

                    self.write_new_status(current_status = current_status, 
                                    csv_writer = csv_writter)
            else:
                print(f"{current_status[0].strftime(DATETIME_FORMAT)}; {self._hostname}; {current_status[1]}; {current_status[2]}")

            if (current_status[2] == STATUS_UP):
                self.execute_post_action(self._up_action)
            else:
                self.execute_post_action(self._down_action)

            if self._interval_in_secounds:
                time.sleep(self._interval_in_secounds)
            else:
                break

    def execute_post_action(self, post_action):
        if (not post_action is None):
            if self._debug:
                print("Executing action: " +  post_action)
            os.system(post_action)

